# Workflow "execute Test, then publish results"

### Definition 


### Steps of the workflow 
1. **getTests** - Get tests code from bucket s3
2. **executeTests** - Execute Tests
3. **publishResults** - Publish results into mysql database

## Main commands

### Create Workflow Template
```
$ argo template create buildTestthenPublish.yaml -n argo
Name:                workflow-template-execute-test
Namespace:           argo
Created:             Wed Aug 26 11:41:24 +0200 (now)
```
### Execute Workflow Template
```
$ argo submit -n argo -p REPO=simpletest -p VERSION=v0.5.0 --watch --from workflowtemplate/workflow-template-execute-test
```
### Execute Workflow 
```
$ argo submit -n argo --parameter-file arguments-parameters.yaml --watch buildTestthenPublish.yaml
Name:                workflow-template-execute-test-4bdbr
Namespace:           argo
ServiceAccount:      default
Status:              Succeeded
Conditions:          
 Completed           True
Created:             Fri Aug 28 10:59:47 +0200 (34 seconds ago)
Started:             Fri Aug 28 10:59:47 +0200 (34 seconds ago)
Finished:            Fri Aug 28 11:00:21 +0200 (now)
Duration:            34 seconds
Parameters:          
  REPO:              simpletest
  VERSION:           v0.5.0
  APPLICATION:       myapp
  BROWSER:           firefox

STEP                                     TEMPLATE        PODNAME                                          DURATION  MESSAGE
 ✔ workflow-template-execute-test-4bdbr  executeTest                                                                  
 ├-✔ getTestsFiles                       getTestsFiles   workflow-template-execute-test-4bdbr-767932743   5s          
 ├-✔ executeRFTest                       executeRFTest   workflow-template-execute-test-4bdbr-1604028564  21s         
 └-✔ publishResults                      publishResults  workflow-template-execute-test-4bdbr-4129469102  4s
```
### Port forward service, pod for accessing UI in local
```
$ kubectl port-forward svc/pgadmin-pgadmin4 8000:80 -n insights
Forwarding from 127.0.0.1:8000 -> 80
Forwarding from [::1]:8000 -> 80
```
### Check logs
```
$ kubectl logs -n argo -c main  workflow-template-execute-test-
==============================================================================
Tests                                                                         
==============================================================================
Tests.Test                                                                    
==============================================================================
Tests.Test.Simpletest                                                         
==============================================================================
Visit Bing                                                            | PASS |
[...]
```
### Get Postgres IP
```
$ kubectl get svc -n insights postgres-postgresql -o=jsonpath="{.spec.clusterIP}"
10.100.9.87
```

## CICD

### Convert secret in base64
```
$ echo -n 'xxx' | base64
eHh4
```
### Create AWS Secret 
```
$ kubectl apply -n argo -f aws_creds.yaml
secret/aws-creds created
```
### Create configmap within postgres creds
```
$ kubectl apply -n argo -f postgresdb_config.yaml
configmap/db-config created
```
## Configuration of postgres db and grafana 
[robot-framework-test-results-in-grafana-postgresql](https://cognitiveqe.com/robot-framework-test-results-in-grafana-postgresql/)

# How to add a new customer 
* Create a new repository within tests-repo group. This repository will be used for storing the tests. The customer will have **full rights : write and push the code**.
* Create a new repository within executetestrf group. The pipeline of this repo will execute the test. The customer will have **limited rights : trigger the pipeline thanks to the api**. 
We need to configure a webhook (settings > webhook) linked to the ci pipeline of the client and allow trigerring the pipeline through the api (settings > cicd > pipeline triggers)
* The manual tasks listed below has te be executed for each customer 

### Manual tasks for configuring insights

**PGAdmin**
- Create a new postgres database
- Create a View named in PostgreSQL DB (code below). The job has to be executed one before creating the view - because the job will configure the database. 
  
**Grafana**
- Add the postgres datasource
- Import the grafana dashboard for postgres database
- Create an account only limited for seing the graphic dedicated to the customer

The code of the view to create defined below :
```
SELECT b.start_time,
    b.suite_name,
        CASE
            WHEN b.passed IS NULL THEN 0::bigint
            ELSE b.passed
        END AS passed,
        CASE
            WHEN c.failed IS NULL THEN 0::bigint
            ELSE c.failed
        END AS failed,
    b.suite_execution_time
   FROM ( SELECT a.suite_id,
            a.start_time,
            a.suite_name,
            count(a.test_execution_status) AS passed,
            a.suite_execution_time
           FROM ( SELECT s.id AS suite_id,
                    s.name AS suite_name,
                    sr.start_time,
                    sr.execution_status AS suite_status,
                    tr.execution_status AS test_execution_status,
                    tc.id AS test_id,
                    tc.name AS test_name,
                    tr.execution_elapsed AS test_elapsed_time,
                    sr.elapsed AS suite_execution_time
                   FROM suite s
                     JOIN suite_result sr ON s.id = sr.suite_id
                     JOIN test_result tr ON sr.test_run_id = tr.test_run_id
                     JOIN test_case tc ON tr.test_id = tc.id
                  ORDER BY sr.start_time DESC) a
          WHERE a.test_execution_status = 'PASS'::text
          GROUP BY a.suite_id, a.start_time, a.suite_name, a.suite_execution_time) b
     FULL JOIN ( SELECT a.suite_id,
            a.start_time,
            a.suite_name,
            count(a.test_execution_status) AS failed,
            a.suite_execution_time
           FROM ( SELECT s.id AS suite_id,
                    s.name AS suite_name,
                    sr.start_time,
                    sr.execution_status AS suite_status,
                    tr.execution_status AS test_execution_status,
                    tc.id AS test_id,
                    tc.name AS test_name,
                    tr.execution_elapsed AS test_elapsed_time,
                    sr.elapsed AS suite_execution_time
                   FROM suite s
                     JOIN suite_result sr ON s.id = sr.suite_id
                     JOIN test_result tr ON sr.test_run_id = tr.test_run_id
                     JOIN test_case tc ON tr.test_id = tc.id
                  ORDER BY sr.start_time DESC) a
          WHERE a.test_execution_status = 'FAIL'::text
          GROUP BY a.suite_id, a.start_time, a.suite_name, a.suite_execution_time) c ON b.start_time = c.start_time AND b.suite_name = c.suite_name;
```